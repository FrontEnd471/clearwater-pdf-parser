TERMS SHEET
This terms sheet contains selected information for quick reference only. You should read this Supplement, 
particularly “Risk Factors,” and each of the other documents listed under “Available Information.”
Sponsor: Deutsche Bank Securities Inc.
Co-Sponsor: Loop Capital Markets LLC
Trustee: Wells Fargo Bank, N.A.
Tax Administrator: The Trustee
Closing Date: June 30, 2009
Distribution Dates: For the Group 1, 3 and 5 Securities, the 16th day of each month or, if the 16th day is 
not a Business Day, the first Business Day thereafter, commencing in July 2009. For the Group 2 and 4
Securities, the 20th day of each month or, if the 20th day is not a Business Day, the first Business Day 
thereafter, commencing in July 2009.
Trust Assets:
Original Term
Trust Asset<Data>To Maturity
Group Trust Asset Type Certificate Rate<Data>    (in years)
?      Ginnie Mae I 4.50%<Data>?Ω
?<Data>     Ginnie Mae I 5.50%<Data>??
?<Data>Ginnie Mae II 5.50%<Data>?
?
?<Data>     Ginnie Mae I 6.00%
?
??
? Ginnie Mae II 6.00%<Data>??
          
??


