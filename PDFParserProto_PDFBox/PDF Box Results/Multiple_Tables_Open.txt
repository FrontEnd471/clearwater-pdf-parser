BASIC BALANCES AND INTERESTS
As Prepared for sample
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non nisl viverra,
bibendum diam vitae, porta eros. Donec tempus posuere porta. Aliquam non vestibulum
magna. Suspendisse feugiat gravida enim, ac suscipit tellus facilisis at. Nulla malesuada
orci eu tellus tempor, eu vulputate mauris blandit. Etiam pharetra fermentum tortor, eget
euismod diam ullamcorper eget. Curabitur finibus quis lacus in pulvinar. Phasellus
laoreet ex massa, non congue justo faucibus et. Nunc rutrum, elit et imperdiet tincidunt,
tellus magna consequat est, eget volutpat lectus purus sit amet arcu. Sed congue ligula
et vestibulum lobortis. Vivamus laoreet venenatis purus vitae tempus. Morbi sem neque,
ullamcorper quis diam et, consequat gravida nibh. Donec eleifend elementum luctus.
Suspendisse sodales nisl in consectetur fermentum. Donec suscipit elementum magna.
Minimum Interest<Data>Term(Months)<Data>Balance<Data>Lender$21.20<Data>3.5%<Data>6<Data>$233.39<Data>Citi$3.39<Data>4.5%<Data>144<Data>$3.39 Ford$14.00<Data>1.5%<Data>12<Data>$233.00<Data>BarClay$50.00<Data>3.55%<Data>12<Data>$2333.39<Data>Apple$310.33<Data>3.99%<Data>72<Data>$23453.39<Data>Volkswagon$75.10<Data>23.49%<Data>24 $2331.39<Data>ICCU$212.33<Data>15.99%<Data>60 $9233.39<Data>Bank of America
Etiam eleifend viverra turpis non blandit. Aliquam suscipit sapien nec metus vulputate,
ac congue est bibendum. Praesent at rutrum tortor, a viverra turpis. Donec auctor mi nec
ante consectetur ultrices. Mauris mattis ante nulla, quis mattis erat pulvinar sit amet. In
vel viverra dolor. Curabitur iaculis mollis egestas. Aenean felis lacus, semper et
fermentum ac, pretium vel nisl. Donec tincidunt mauris nec tortor pharetra sagittis. Nulla
facilisi. Vestibulum et cursus turpis, ac pretium justo. Pellentesque habitant morbi
tristique senectus et netus et malesuada fames ac turpis egestas.
 
TERMS SHEET
This terms sheet contains selected information for quick reference only. You should read this
Supplement, particularly “Risk Factors,” and each of the other documents listed under “Available
Information.”
Sponsor: Deutsche Bank Securities Inc.
Co-Sponsor: Loop Capital Markets LLC
Trustee: Wells Fargo Bank, N.A.
Tax Administrator: The Trustee
Closing Date: June 30, 2009
Distribution Dates: For the Group 1, 3 and 5 Securities, the 16th day of each month or, if the 16th day
is not a Business Day, the first Business Day thereafter, commencing in July 2009. For the Group 2 and 4 
Securities, the 20th day of each month or if the 20th day is not a Business Day, the first Business Day
thereafter, commencing in July 2009.
Trust Assets:
  Original Term
Trust Asset<Data>    To Maturity
    Group Trust Asset Type   Certificate Rate            (in years)
        1 Ginnie Mae I 4.5% 30
                    2 Ginnie Mae II 5.5% 30
                    3 Ginnie Mae I 5.5% 30
                    4 Ginnie Mae II 6.0% 30
                    5 Ginnie Mae I 6.0% 30
Security Groups: This series of Securities consists of multiple Security Groups (each, a “Group”), as shown on 
the front cover of this Supplement and on Schedule I to this Supplement. Payments on each Group will be based 
solely on payments on the Trust Asset Group with the same numerical designation.
Assumed Characteristics of the Mortgage Loans Underlying the Trust Assets1:
    Weighted Average<Data>Weighted Average<Data>Weighted
Principal<Data>     Remaining Term<Data>       Loan Age<Data>Average
Balance  2 to Maturity (in months)<Data>      (in months)<Data>      Mortgage Rate  3 
Group 1 Trust Assets
$250,000,000     358    2     5.0%
Group 2 Trust Assets
$176,673,486     350    8     6.0%
Group 3 Trust Assets
$136,367,164     343  14     6.0%
Group 4 Trust Assets
$319,141,386     350    8     6.5%
Group 5 Trust Assets
$60,000,000     339  20     6.5%
________________
1 As of June 1, 2009.


