TERMS SHEET 
 
This terms sheet contains selected information for quick reference only. You should read this Supplement, 
particularly “Risk Factors,” and each of the other documents listed under “Available Information.” 
 
Sponsor: Deutsche Bank Securities Inc. 
Co-Sponsor: Loop Capital Markets LLC 
Trustee: Wells Fargo Bank, N.A. 
Tax Administrator: The Trustee 
Closing Date: June 30, 2009 
 
Distribution Dates: For the Group 1, 3 and 5 Securities, the 16th day of each month or, if the 16th day is not a 
Business Day, the first Business Day thereafter, commencing in July 2009. For the Group 2 and 4 Securities, the 
20th day of each month or if the 20th day is not a Business Day, the first Business Day thereafter, commencing 
in July 2009. 
 
Trust Assets: 
           Original Term 
 Trust Asset           To Maturity 
     Group  Trust Asset Type    Certificate Rate            (in years) 
 
        ?    Ginnie Mae I    4.5%    ?? 
                    ?   Ginnie Mae II    5.5%    ?? 
                    ?<Data>   Ginnie Mae I    5.5%    ? 
                    ?   Ginnie Mae II    6.0%    ? 
                    ?<Data>   Ginnie Mae I    6.0%    ??<Data>?<Data> 
 
Security Groups: This series of Securities consists of multiple Security Groups (each, a “Group”), as shown on 
the front cover of this Supplement and on Schedule I to this Supplement. Payments on each Group will be based 
solely on payments on the Trust Asset Group with the same numerical designation. 
 
Assumed Characteristics of the Mortgage Loans Underlying the Trust Assets1: 
 
       Weighted Average  Weighted Average  Weighted 
 Principal      Remaining Term         Loan Age   Average
 Balance2  to Maturity (in months)       (in months)       Mortgage Rate3 
Group 1 Trust Assets   
$250,000,000        ?Ω         2        5.0% 
Group 2 Trust Assets 
$176,673,486        ‡?<Data>?       8        6.0% 
Group 3 Trust Assets 
$136,367,164         ¥§®     14        6.0% 
Group 4 Trust Assets 
$319,141,386        350      ??<Data>         6.5% 
Group 5 Trust Assets 
$60,000,000        339       ??        6.5% 
________________ 
1 As of June 1, 2009. 


